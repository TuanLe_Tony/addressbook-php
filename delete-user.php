<?php
	// 	1. separate out your post & get request
	if ($_SERVER["REQUEST_METHOD"] == "GET") {
		//	2. write code for GET request
		echo "<h1> I GOT GET REQUEST! </h1>";

		$id = $_GET["id"];

		include_once 'database.php';
		// 3.  MAKE a SQL QUERY
		$query = "SELECT * from USERS where id=" . $id;

		$results = mysqli_query($con, $query);

		// comes from the database
		$users = mysqli_fetch_assoc($results);

		print_r($users);
	}
	else if ($_SERVER["REQUEST_METHOD"] == "POST") {
		// 	3. write code for POST request
		echo "<h1 margin-top='100px'> I GOT POST REQUEST! </h1>";

		// get the data from the form
		$id = $_POST["user_id"];

		include_once 'database.php';

		// 3.  MAKE a SQL QUERY
		$query = 	"DELETE FROM USERS ";
		$query .= 	"WHERE id=" . $id;

		$results = mysqli_query($con, $query);

		if ($results) {
			echo "OKAY! <br>";

			// command to redirect back to show-products page.
			header("Location: profile.php");

		}
		else {
			echo "BAD! <br>";
			echo mysqli_error($con);
		}
	}
?>
<!DOCTYPE html5>
<html>
<head>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
	<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
	<style type="text/css">
		.mdl-grid {
			max-width:1024px;
			margin-top:40px;
		}

		h1 {
			font-size:36px;
		}
		h2 {
			font-size:30px;
		}
	</style>

</head>
<body>
	<div class="mdl-grid" align="center">
	  <div class="mdl-cell mdl-cell--12-col">
	  	<h1> Delete Users </h1>
		<h2> Are you sure you want to delete? </h2>

		<h3> First Name: <?php echo $users["firstname"] ?> </h3>
		<h3> Last name: <?php echo $users["lastname"] ?> </h3>
		<h3> Email Adress: <?php echo $users["email"] ?> </h3>
    <h3> Phone Number: <?php echo $users["phonenumber"] ?> </h3>

		<!-- form -->
		<!-- @TODO: Update your form action/method here -->
		<form action="delete-user.php" method="POST">
			<input
				name="user_id"
				value="<?php echo $id; ?>"
				hidden=true
			>
		  <!-- @TODO: Update the link  -->
		  <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent">
			Yes
		  </button>
		</form>

		<br>

		<a href="profile.php" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
			No
		</a>
	  </div>
	</div>

</body>
</html>
