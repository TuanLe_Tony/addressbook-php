<?php
session_start();
include_once 'database.php';
$sql= "SELECT * FROM USERS ";
$result = mysqli_query($con,$sql);
$check = mysqli_fetch_array($result);
//END of DATABASE METHOD HERE.
 ?>
 <!DOCTYPE html>
 <html>
   <head>
     <title>Address Book App</title>
	   <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
     <style type = "text/css">
       #label {
         color: green;
         margin-top: 100px;
       }
       #label h1 {
         letter-spacing: 2px;
         font-family: serif;
       }
       #fillinformation {
         margin-top: 100px;
       }
       #confirm{
         padding: 10px 100px 10px 100px;
       }
     </style>
   </head>
   <body>
     <div align="center" id="label">
       <h1>ADDRESS BOOK APP</h1>
     </div>

     <div class="mdl-grid" align="center">
      <div class="mdl-cell mdl-cell--12-col">
         <h1> Book List Users </h1>
         <h2> Users's Information are shown below: </h2>

       <!-- USERS table -->
       <!-- @TODO: Insert PHP code here -->
       <div class="container">
         <table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
           <thead>
           <tr>
             <th class="mdl-data-table__cell--non-numeric" align="center">First Name</th>
             <th class="mdl-data-table__cell--non-numeric">Last Name</th>
             <th class="mdl-data-table__cell--non-numeric">Email Address</th>
             <th class="mdl-data-table__cell--non-numeric">Phone Number</th>
           </tr>
           </thead>
           <tbody>
       <?php
      //@TODO: You need to jumble PHP and HTML code here.

      // loop through the database results
      while( $users = mysqli_fetch_assoc($result) ) {
        echo "<tr>";
          echo "<td class='mdl-data-table__cell--non-numeric'>";
            echo $users["firstname"];
          echo "</td>";
          echo "<td class='mdl-data-table__cell--non-numeric'>";
            echo $users["lastname"];
          echo "</td>";
          echo "<td>";
            echo $users["email"];
          echo "</td>";
          echo "<td>";
            echo $users["phonenumber"];
          echo "</td>";
          echo "<td>";
            echo "<a href='edit-users.php?id=" . $users["id"] . "'>";
              echo "Edit";
            echo "</a>";
          echo "</td>";
          echo "<td>";
            echo "<a href='delete-user.php?id=" . $users["id"] . "'>";
              echo "Delete";
            echo "</a>";
          echo "</td>";
        echo "</tr>";

      }
      ?>

          </tbody>
        </table>
        <!-- end table -->
      </div>



     <script>

     </script>
   </body>
 </html>
