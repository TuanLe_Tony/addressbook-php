
<!DOCTYPE html>
<html>
  <head>
    <title>Address Book App</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <style type = "text/css">
      #label {
        color: green;
        margin-top: 100px;
      }
      #label h1 {
        letter-spacing: 2px;
        font-family: serif;
      }
      #fillinformation {
        margin-top: 100px;
      }
      #confirm{
        padding: 10px 100px 10px 100px;
      }
    </style>
  </head>
  <body>
    <div align="center" id="label">
      <h1>ADDRESS BOOK APP</h1>
    </div>

    <form method="POST" action="AddInformation.php">
      <div class="container" id="fillinformation">
        <form class="was-validated">
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="inputfirstname" >First name</label>
              <input name= "firstname" type="text" class="form-control is-valid" id="inputfirstname" placeholder="First Name" required>
            </div>
            <div class="form-group col-md-6">
              <label for="inputlastname">Last Name</label>
              <input name="lastname" type="text" class="form-control is-valid" id="inputlastname" placeholder="Last Name" required>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="inputEmail4">Email Addres</label>
              <input name="email" type="email" class="form-control is-valid" id="inputEmail4" placeholder="Email Address" required>
            </div>
            <div class="form-group col-md-6">
              <label for="inputphone">Phone Number</label>
              <input name="phonenumber" type="number" class="form-control is-valid" id="inputphone" placeholder="Phone Number" required>
            </div>
          </div>
          <div align="center">
            <button type="submit" class="btn btn-primary" id="confirm">CONFIRM</button>
          </dive>
      </form>
    </div>

    <script>

    </script>
  </body>
</html>
