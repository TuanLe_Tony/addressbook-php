<?php
	// 	1. separate out your post & get request
	if ($_SERVER["REQUEST_METHOD"] == "GET") {
		//	2. write code for GET request
		echo "<h1> I GOT GET REQUEST! </h1>";

		$id = $_GET["id"];

	  include_once 'database.php';

		// 3.  MAKE a SQL QUERY
		$query = "SELECT * from USERS where id=" . $id;

		$results = mysqli_query($con, $query);

		// you can do this becuse there's only 1 result that
		// comes from the database
		$users = mysqli_fetch_assoc($results);

	}
	else if ($_SERVER["REQUEST_METHOD"] == "POST") {

		// 	3. write code for POST request
		echo "<h1> I GOT POST REQUEST! </h1>";

		// get the data from the form
		$id = $_POST["user_id"];
    $firstname = $_POST["firstname"];
		$lastname = $_POST["lastname"];
		$email = $_POST["email"];
    $phone = $_POST["phonenumber"];

		// 1. WHAT IS YOUR INFO?

    include_once 'database.php';
		// 3.  MAKE a SQL QUERY

		$query = 	" UPDATE USERS ";
		$query .= 	"SET ";
		$query .= 	"firstname= '" . $firstname . "', ";
		$query .= 	"lastname='" . $lastname . "', ";
		$query .= 	"email='" . $email . "', " ;
    $query .= 	"phonenumber='" . $phone . "' " ;
		$query .=	"WHERE ";
		$query .= 	"id=" . $id;

		echo $query ."<br>";

		$results = mysqli_query($con, $query);

    if ($results) {
      echo "OKAY! <br>";

      // command to redirect back to show-products page.
      header("Location: profile.php");

    }
    else {
      echo "BAD! <br>";
      echo mysqli_error($con);
    }

  }

?>
<!DOCTYPE html5>
<html>
<head>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
	<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
	<style type="text/css">
		.mdl-grid {
			max-width:1024px;
			margin-top:40px;
		}

		h1 {
			font-size:36px;
		}
		h2 {
			font-size:30px;
		}
	</style>

</head>
<body>

	<div class="mdl-grid" align="center">
	  <div class="mdl-cell mdl-cell--12-col">
	  	<h1> Edit Users </h1>
		<h2> Edit your users info below: </h2>

		<!-- form -->
		<!-- @TODO: Update your form action/method here -->
		<form action="edit-users.php" method="POST">
			<input name="user_id"
					value="<?php echo $id; ?>"
					hidden=true
			>


			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
				<!-- @TODO: update NAME and ID! -->
				<input 	name="firstname"
						class="mdl-textfield__input"
						type="text"
						id="sample3"
						value="<?php echo $users["firstname"]; ?>">


				<label class="mdl-textfield__label" for="sample3">First Name</label>
			</div>
			<br>
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
				<!-- @TODO: update NAME and ID! -->
				<input	name="lastname"
						class="mdl-textfield__input"
						type="text"
						id="sample3"
						value="<?php echo $users["lastname"]; ?>">

				<label class="mdl-textfield__label" for="sample3">Last Name</label>
			</div>
			<br>
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
				<!-- @TODO: update NAME and ID! -->
				<input 	name="email"
						class="mdl-textfield__input"
						type="email"
						id="sample2"
						value="<?php echo $users["email"]; ?>"
						>
				<label class="mdl-textfield__label" for="sample2">Email</label>
				<span class="mdl-textfield__error">Input is not a Email type!</span>
			</div>
			<br>
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
				<!-- @TODO: update NAME and ID! -->
				<input 	name="phonenumber"
						class="mdl-textfield__input"
						type="number"
						id="sample2"
						value="<?php echo $users["phonenumber"]; ?>"
						>
				<label class="mdl-textfield__label" for="sample2">Phone Number</label>
				<span class="mdl-textfield__error">Input is not a number!</span>
			</div>
      <br>


		  <!-- @TODO: Update the link  -->
		  <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent">
			Update
		  </button>
		</form>

		<br>

		<a href="profile.php" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
			< Go Back
		</a>
	  </div>
	</div>

</body>
</html>
